"""."""

from django.shortcuts import render


def index(request):
    """."""
    # set session['id'] to 42
    # can be accessed via django.contrib.sessions.backends.db.SessionStore
    request.session['id'] = '42'
    # set session/cookie expiry date to 100 years
    request.session.set_expiry(60 * 60 * 24 * 365 * 100)
    context = {'a': 'b'}
    return render(request, 'polls/index.html', context)


def get_session(request):
    """."""
    # get session id
    if request.session.exists(request.session.session_key):
        print(type(request.session))
        print(request.session.session_key)
    context = {'a': 'b'}
    return render(request, 'polls/index.html', context)
